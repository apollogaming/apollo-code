//Shield Generator - Emitters
//The shield emitters



//
// Emitter Base Class
//

/obj/machinery/shielding/emitter
	icon = 'icons/obj/machines/shieldgen.dmi'
	anchored = 1
	level = 1
	layer = 2.3
	blend_mode = BLEND_ADD
	var/online = 0
	var/control = 0
	var/list/obj/shielding/shield/PoweredShields = list( )
	use_power = 1
	alpha = 250
	idle_power_usage = 75 //Watts, I hope.  Just enough to do the computer and display things.


//When an emitter is created, run the AoE setup code after a delay
/obj/machinery/shielding/emitter/New()
	spawn(5)
		UpdateAoE()
	..()


/obj/machinery/shielding/emitter/proc/UpdateAoE()
	return


//Whether the emitter is powered - remember, this uses shield power, not electrical power!
/obj/machinery/shielding/emitter/powered()
	return ShieldNetwork && ShieldNetwork.HasPower()


//Basic process function
/obj/machinery/shielding/emitter/process()
	var/nonline = ShieldNetwork.HasPower() && control
	if (online != nonline)
		for(var/obj/shielding/shield/Shield in PoweredShields)
			if(Shield.disabled)
				Shield.density = 0
				Shield.invisibility = 101
				Shield.explosionstrength = 0
				continue
			if (nonline)
				Shield.density = !Shield.blockatmosonly
				Shield.icon_state = "shieldsparkles"
				Shield.explosionstrength = INFINITY
				Shield.invisibility = 0
			else
				Shield.density = 0
				Shield.invisibility = 101
				Shield.explosionstrength = 0

	online = nonline
	if(online)
		use_power = 2
		active_power_usage = PoweredShields.len * 10
	else
		use_power = 1

	icon_state = online ? "plate" : "plate-p" //TODO better graphics handling


//Called when a shield tile requires additional power
//e.g. after an explosion (I might put stats-tracking in here...)
/obj/machinery/shielding/emitter/proc/Draw(amount)


//Set up a shield tile on this turf if applicable
/obj/machinery/shielding/emitter/proc/UpdateTurf(var/turf/S)
	for (var/obj/shielding/shield/shield in S)
		if(!shield.emitter || shield.emitterdist < get_dist(shield, src))
			shield.emitter = src
			shield.emitterdist = get_dist(shield, src)
		return

	for(var/dir in alldirs)
		var/turf/T = get_step(S,dir)
		if(T)
			if(T.type != /turf/space)
				AddShield(S)
				return

/obj/machinery/shielding/emitter/proc/AddShield(var/turf/S)
	if(istype(S, /turf/simulated/floor))
		return
	for(var/turf/simulated/wall/W in orange(S, 1))
		var/counter
		if(W && (get_dir(S, W) == NORTH || SOUTH || WEST || EAST))
			counter++
			if(counter >= 3)
				return

	for(var/turf/simulated/floor/plating/P in orange(S, 1))
		var/counter
		if(P && (get_dir(S, P) == NORTH || SOUTH))
			counter++
			if(counter >= 2)
				return

	for (var/obj/shielding/shield/shield in S)
		if(!shield.emitter)
			shield.emitter = src
		return
	sleep(-1)
	var/obj/shielding/shield/Shield = new /obj/shielding/shield(S)
	Shield.emitter = src
	PoweredShields += Shield
	return

//
// Plate-Type Emitter
//


/obj/machinery/shielding/emitter/plate
	icon_state = "plate"
	name = "Emitter Plate"
	desc = "An AoE shield emitter"
	level = 1
	use_power = 2
	var/range_dist = 10

//AoE Setup for the emitter plate - Shield the exterior hull
/obj/machinery/shielding/emitter/plate/UpdateAoE()
	for(var/obj/shielding/shield/shield in PoweredShields)
		shield.density = 0
		shield.invisibility = 101
		shield.explosionstrength = 0
		shield.emitter = null
		shield.emitterdist = INFINITY

	for(var/turf/space/S in trange(src,range_dist))
		UpdateTurf(S)


/obj/machinery/shielding/emitter/plate/New()
	..()

	var/turf/T = src.loc			// hide if turf is not intact

	if(level==1) hide(T.intact)


/obj/machinery/shielding/emitter/plate/hide(var/i)
	if(level == 1 && istype(loc, /turf/simulated))
		invisibility = i ? 101 : 0
	updateicon()

/obj/machinery/shielding/emitter/plate/proc/updateicon()
	return